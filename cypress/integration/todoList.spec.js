import TodoList from '../support/pages/TodoList'

describe('Validar Todos List', () => {
    it('Cadastrar Item Um na lista', () => {
        TodoList.accessTodoList();
        TodoList.fillItem('ItemUm');
        TodoList.selectFilter('All');
        expect(TodoList.getItem(0, 'ItemUm'));
    });

    it('Cadastrar Item Dois na lista', () => {
        TodoList.fillItem('ItemDois');
        expect(TodoList.getItem(1, 'ItemDois'));
    });

    it('Cadastrar Item Tres na lista', () => {
        TodoList.fillItem('ItemTres');
        expect(TodoList.getItem(2, 'ItemTres'));
    });

    it('Cadastrar Item Quatro na lista', () => {
        TodoList.fillItem('ItemQuatro');
        expect(TodoList.getItem(3, 'ItemQuatro'));
    });

    it('Remover Item Um da lista', () => {
        TodoList.deleteItem(0);
        expect(TodoList.getCount(3));
    });

    it('Remover Item Dois da lista', () => {
        TodoList.deleteItem(0);
        expect(TodoList.getCount(2));
    });

    it('Marcar um Item como completo', () => {
        TodoList.selectItem(0);
        expect(TodoList.getItemCompleted(0));
    });

    it('Selecionar Filtro "Active"', () => {
        TodoList.selectFilter('Active');
        expect(TodoList.getItem(0, 'ItemQuatro'));
    });

    it('Selecionar Filtro "Completed"', () => {
        TodoList.selectFilter('Completed');
        expect(TodoList.getItem(0, 'ItemTres'));
    });

    it('Selecionar Filtro "All" e clicar em "Clear completed"', () => {
        TodoList.selectFilter('All');
        TodoList.selectClearCompleted();  
        expect(TodoList.getItem(0, 'ItemQuatro'));
    });
});