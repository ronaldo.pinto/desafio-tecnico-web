/// <reference types="cypress" />

const elements = require('./elements').ELEMENTS;

class TodoList {
    accessTodoList(){
        cy.visit('https://todomvc.com/examples/typescript-react/#/completed');
    }
    fillItem(item){
        cy.get(elements.new)
            .type(item)
            .type('{enter}');
    }
    getItem(position, item){
        cy.get(elements.item)
            .eq(position)
            .find('label')
            .should('contain', item);
    }
    selectFilter(filter){
        cy.get(elements.filter)
            .contains(filter)
            .click();
    }
    deleteItem(position){
        cy.get(elements.delete)
            .eq(position)
            .click({ force: true });
    }
    getCount(value){
        cy.get(elements.count)
            .should('contain.text', value);
    }
    selectItem(position){
        cy.get(elements.complete)
            .eq(position)
            .click();
    }
    getItemCompleted(position){
        cy.get(elements.list)
            .eq(position)
            .should('contain.html', 'completed');
    }
    selectClearCompleted(){
        cy.get(elements.clearCompleted)
            .click();
    }
}

export default new TodoList();