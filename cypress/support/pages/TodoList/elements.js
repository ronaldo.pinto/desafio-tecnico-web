export const ELEMENTS = {
    new: '.new-todo',
    list: '.todo-list',
    item: '.todo-list li',
    filter: '.filters',
    delete: '.destroy',
    count: '.todo-count strong',
    complete: '.toggle',
    clearCompleted: '.clear-completed'
}